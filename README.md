# iDempiere Plugin Generator

This project creates an iDempiere plugin skeleton.

- Plugin example [com.ingeint.template](com.ingeint.template).

- Target platform example https://bitbucket.org/ingeint/ingeint-idempiere-target-platform/

## Getting Start

Create a new plugin with the command `make`.
